from gevent.pywsgi import WSGIServer
import werkzeug.serving

from app.factory import create_app


@werkzeug.serving.run_with_reloader
def run_server():
    app = create_app()

    ws = WSGIServer(('0.0.0.0', 8310), app)
    ws.serve_forever()
