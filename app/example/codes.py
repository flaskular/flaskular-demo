"""
Defines the codes and messages which are used by the server to respond to the
client.
"""
from flaskular.codes import CodeManager


# Keys are the code id, values are the code message
_code_dict = {
    # Unknown
    'unknown': 'An unknown server error has occured in the examples module',

    'hello_success': 'This succeeded with msg = {a}'

}

# Auto-register codes
EXAMPLE_CODES = CodeManager(_code_dict)
