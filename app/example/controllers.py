from flask import Blueprint, request
from flaskular.decorators import auth_required, json_post

from .codes import EXAMPLE_CODES as c


example = Blueprint('example', __name__, url_prefix='/example')


@example.route('/hello', methods=['POST'])
@json_post()
@auth_required()
def hello():
    foo = request.json['foo']

    return {
        'code': c.hello_success(a='I win!'),
        'my_msg': 'Hello World: ' + foo
    }
