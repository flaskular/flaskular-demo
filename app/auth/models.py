from flaskular.auth import FlaskularUserMixin, FlaskularRoleMixin

from app.extensions import db


# 00000000000000000000000000000000000000000000000000000000000000000000000000000
#   Users <-> Roles link
# 00000000000000000000000000000000000000000000000000000000000000000000000000000

roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('auth_user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('auth_role.id'))
)


# 00000000000000000000000000000000000000000000000000000000000000000000000000000
#   Role Model
# 00000000000000000000000000000000000000000000000000000000000000000000000000000

class Role(FlaskularRoleMixin):
    """Model defining a user role.

    Schema
    ------
    id : int, primary key
    created_at : DateTime
    modified_at : DateTime
    name : str, unique, not null
    description : str

    Parameters
    ----------
    name : str
        Sets name in schema.
    """
    __tablename__ = 'auth_role'

    def __init__(self, name):
        FlaskularRoleMixin.__init__(self, name)

    def __repr__(self):
        return '<Role {}>'.format(self.name)

    ###########################################################################
    #   Static Helpers
    ###########################################################################

    @staticmethod
    def initialize():
        """Initializes the database with default roles.

        The inherited FlaskularRole will create the `admin` and `subscriber`
        roles. All other roles created here.
        """
        # Create admin and subscriber roles
        FlaskularRoleMixin.initialize()

        # Create custom roles here
        # ...

        # Other custom initialization scripts here
        # ...

    @staticmethod
    def create(name, description='', commit=True):
        """
        Creates and returns a new role.

        Parameters
        ----------
        name : str
            The name of the new role.
        description : str, default=''
            The (optional) description of the new role.
        commit : bool, default=True
            Whether the new role should be committed to the database upon
            creation.

        Returns
        -------
        role : Role
            The new role.
        """
        # Set name and description
        role = FlaskularRoleMixin.create(name, description, commit=commit)

        # Set other custom attributes here
        # ...

        if commit:
            db.session.commit()
        else:
            db.session.close()

        return role


# 00000000000000000000000000000000000000000000000000000000000000000000000000000
#   User Model
# 00000000000000000000000000000000000000000000000000000000000000000000000000000

class User(FlaskularUserMixin):
    """Model defining a user.

    Schema
    ------
    id : int, primary key
    created_at : DateTime
    modified_at : DateTime
    email : str, unique, not null
    password : str, not null
    first_name : str
    last_name : str
    active : bool
    confirmed_at : DateTime
    last_login_at : DateTime
    current_login_at : DateTime
    last_login_ip : str
    login_count : int
    logged_out : boolean
        Tracks whether a logout has recently been executed. Used to prevent
        token logins when the user has manually logged out.
    roles : relationship (many-to-many with role)
    """
    __tablename__ = 'auth_user'

    roles = db.relationship('Role',
                            secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __repr__(self):
        return '<User {}>'.format(self.email)

    ###########################################################################
    #   Methods
    ###########################################################################

    def to_dict(self):
        """Returns a dictionary representation of this user.

        Returns
        -------
        user_dict : dict
        """
        d = FlaskularUserMixin.to_dict(self)

        # Add custom dictionary attributes here
        # ...

        return d

    ###########################################################################
    #   Static Helpers
    ###########################################################################

    @staticmethod
    def initialize():
        """Initializes by creating the default users.

        Creates a user with id=admin@example.com, password=admin if no user
        already exists. Called on application startup.

        Prints a warning to the server log if the admin user is using the
        default password.
        """
        # Create the admin user if no other users exists.
        FlaskularUserMixin.initialize()

        # Custom initialization scripts here, including the creation of default
        # users
        # ...

    @staticmethod
    def create(email, password, roles='subscriber', commit=True):
        """Static helper that creates and returns a new user of the given role.

        Parameters
        ----------
        email : str
            The email identifying the new user
        password : str
            The (unencrypted) password for the new user
        roles : str or list, def='subscriber'
            The name of the role to assign to the user. Must be an entry in the
            roles database.

            If a list is given, all roles in the list are added to the user.
        commit : bool, def=True
            True if the new user should be committed to the database before
            being returned.

        Returns
        -------
        user : User
            The new user object.
        """
        user = FlaskularUserMixin.create(email, password, roles=roles,
                                         commit=commit)

        # Add custom user attributes here
        # ...

        if commit:
            db.session.commit()
        else:
            db.session.close()

        return user
