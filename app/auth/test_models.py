from flask.ext.security.utils import verify_password

from app.test_base import TestModel
from .models import User, Role


class TestAuthModels(TestModel):

    def test_admin_creation(self):
        user = User.query.filter_by(email='admin@example.com').first()
        assert user is not None
        assert isinstance(user, User)
        assert user.email == 'admin@example.com'
        assert verify_password('admin', user.password)

        admin_role = Role.query.filter_by(name='admin').first()
        sub_role = Role.query.filter_by(name='subscriber').first()

        assert isinstance(admin_role, Role)
        assert isinstance(sub_role, Role)

        assert user.has_role(admin_role)
        assert not user.has_role(sub_role)

    def test_role_creation(self):
        roles = Role.query.all()
        assert len(roles) == 2

        role_names = [role.name for role in roles]
        assert 'admin' in role_names
        assert 'subscriber' in role_names

    def test_role_create_new(self):
        role = Role.create('test', 'A test role')

        assert role.name == 'test'
        assert role.description == 'A test role'

        found = Role.query.filter_by(name='test').first()
        assert found is not None
        assert found == role

    def test_role_create_no_store(self):
        role = Role.create('test2', 'Another test role', commit=False)

        assert role.name == 'test2'
        assert role.description == 'Another test role'

        found = Role.query.filter_by(name='test2').first()
        assert found is None

    def test_role_representation(self):
        admin_role = Role.query.filter_by(name='admin').first()
        assert admin_role.name == 'admin'
        assert str(admin_role) == '<Role admin>'

    def test_user_representation(self):
        user = User.query.filter_by(email='admin@example.com').first()
        assert user.email == 'admin@example.com'
        assert str(user) == '<User admin@example.com>'

    def test_create_user(self):
        User.create('test1@example.com', 'test1', 'subscriber')

        found = User.query.filter_by(
            email='test1@example.com'
        ).first()
        assert found is not None
        assert found.email == 'test1@example.com'
        assert verify_password('test1', found.password)

        admin_role = Role.query.filter_by(name='admin').first()
        sub_role = Role.query.filter_by(name='subscriber').first()

        assert not found.has_role(admin_role)
        assert found.has_role(sub_role)

    def test_create_user_multiple_roles(self):
        User.create('test1@example.com', 'test1',
                    roles=['admin', 'subscriber'])

        found = User.query.filter_by(
            email='test1@example.com'
        ).first()
        assert found is not None
        assert found.email == 'test1@example.com'
        assert verify_password('test1', found.password)

        admin_role = Role.query.filter_by(name='admin').first()
        sub_role = Role.query.filter_by(name='subscriber').first()

        assert found.has_role(admin_role)
        assert found.has_role(sub_role)

    def test_create_user_no_store(self):
        user = User.create('test2@example.com', 'test2', 'admin',
                           commit=False)

        assert user.email == 'test2@example.com'
        assert verify_password('test2', user.password)

        admin_role = Role.query.filter_by(name='admin').first()
        sub_role = Role.query.filter_by(name='subscriber').first()

        assert user.has_role(admin_role)
        assert not user.has_role(sub_role)

        found = User.query.filter_by(
            email='test2@example.com'
        ).first()
        assert found is None

    def test_user_to_dict(self):
        admin = User.query.filter_by(email='admin@example.com').first()
        d = admin.to_dict()

        assert d['email'] == 'admin@example.com'
        assert len(d['roles']) == 1
        assert d['roles'][0] == 'admin'
