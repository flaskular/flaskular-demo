from app.test_base import TestModel
from .models import list_all_static_js, list_all_static_css


class TestModels(TestModel):

    def test_css_list(self):
        css_list = list_all_static_css()
        assert 'app' in css_list

        for css in css_list['core'] + css_list['app']:
            assert '.css' in css

        assert 'flaskular.css' in css_list['core']
        assert 'app.css' in css_list['app']

    def test_js_list(self):
        js_list = list_all_static_js()
        assert 'app' in js_list

        for js in js_list['core'] + js_list['app']:
            assert 'lib/' not in js
            assert '.js' in js
            assert 'spec' not in js

        assert 'flaskular.module.js' in js_list['core']
        assert 'auth/auth.module.js' in js_list['core']
        assert 'auth/auth.routes.js' in js_list['core']
        assert 'auth/auth.service.js' in js_list['core']
        assert 'auth/login.controller.js' in js_list['core']
        assert 'auth/register.controller.js' in js_list['core']
        assert 'router/router.provider.js' in js_list['core']

        assert 'app.module.js' in js_list['app']

    def test_js_list_order(self):
        """
        Make sure modules are loaded before everything else.
        """
        js_list = list_all_static_js()
        module_state = True

        for js in js_list['core']:
            print('- ', js)
            if 'module' in js and module_state:
                continue
            if 'module' not in js and module_state:
                module_state = False
                continue
            if 'module' not in js and not module_state:
                continue
            if 'module' in js and not module_state:
                # Module found after some non-module js
                assert False
                break
