from app.test_base import TestController


class TestControllers(TestController):

    def test_angular(self):
        response = self.client.get('/')

        data = response.data.decode('utf-8')
        print(data)

        assert response.status_code == 200
        assert 'html' in data
        assert 'head' in data
        assert 'body' in data

        # make sure some scripts loaded from core and app
        assert 'core/flaskular.module.js' in data
        assert 'core/auth/auth.service.js' in data
        assert 'static/app.module.js' in data

        # make sure some css loaded from core and app
        assert 'core/flaskular.css' in data
        assert 'static/app.css' in data
