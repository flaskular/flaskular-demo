import os

from flaskular import Flaskular
from flaskular.factory import configure_app

from app.extensions import db, security  # noqa
from app.auth import User, Role

from app.angular.controllers import angular  # noqa # allows extension
from app.example.controllers import example

# from app.mymodule.controllers import mymodule


def register_blueprints(app):
    """Utility to register all wired-in blueprints for this app.

    Parameters
    ----------
    app : Flask
        The flask app with which to register blueprints.

    Returns
    -------
    app : Flask
        The flask app in which the blueprints have been registered.
    """
    # Register your modules below:
    # app.register_blueprint(mymodule)
    app.register_blueprint(example)


def create_app(testing=False):
    """Factory to create the standard app.

    Note: to reference the current app from within a module:

            from flask import current app

    Parameters
    ----------
    testing : bool, default=False
        True if the testing configuration and settings should be used.

    Returns
    -------
    app : Flask
        The flask app.
    """
    # Set up flaskular core flaskular application
    app = Flaskular(__name__, instance_path=os.path.dirname(__file__))
    app = configure_app(User, Role, app=app, testing=testing)
    register_blueprints(app)

    # Put custom creation scripts here
    # ...

    return app
