from flaskular.test_base import (TestModel as FlaskularTestModel,
                                 TestController as FlaskularTestController)

from app.factory import create_app


class TestModel(FlaskularTestModel):
    """Base class for tests on flask models.

    Sets up the app and db before each test is run and tears them back down at
    the end (clearing the test db completely).
    """

    _create_app = create_app


class TestController(FlaskularTestController):
    """Base class for tests on flask controllers.

    Sets up the app and db before each test is run and tears them back down at
    the end (clearing the test db completely).

    Also defines helper functions that make testing json posts and responses
    simpler.
    """

    _create_app = create_app
