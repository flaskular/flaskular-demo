/**
 * Routes for the home page.
 */
(function() {
    'use strict';

    angular
        .module('app.home')
        .run(runBlock);

    runBlock.$inject =
        ['routerHelper', '$rootScope', '$state'];

    /**
     * Router initialization.
     */
    function runBlock(routerHelper, $rootScope, $state) {
        routerHelper.configureStates(getStates($state), getAliases());
    }

    /**
     * The default state routes. Overwrites the existing `home` state from
     * flaskular.
     */
    function getStates($state) {
        var home = $state.get('home');
        var config = {
            templateUrl: 'static/home/index.html',
            url: '/',
            controller: 'ExtHomeController',
            controllerAs: 'vm'
        };
        var rtn = [];

        if (home) {
            angular.extend(home, config);
        }
        else {
            rtn = [
                {
                    state: 'home',
                    config: config
                }
            ]
        }

        return rtn;
    }

    /**
     * The aliases for existing states.
     */
    function getAliases() {
        return [
            ['', '/']
        ];
    }
})();
