/**
 * Module definition for home. This is an extension of the flaskular home
 * module, allowing for customization.
 *
 * The home module defines the appearance and behavior of the home page. This
 * page is only accessible when the user is logged out. It provides the login
 * and registration options.
 *
 * @namespace flaskular.home
 */
(function() {
    'use strict';

    angular
        .module('app.home', [
            'ui.router',
            'ui.bootstrap',
            'flaskular.router',
            'flaskular.home'
        ]);
})();
