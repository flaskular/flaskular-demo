/**
 * The extended controller for the home page.
 *
 * @namespace app.home
 */
(function() {
    'use strict';

    angular
        .module('app.home')
        .controller('ExtHomeController', ExtHomeController);

    ExtHomeController.$inject = ['$state', '$controller', 'authService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Extended home controller definition.
     */
    function ExtHomeController($state, $controller, authService) {
        var base = $controller('HomeController', {
            $state: $state, authService: authService
        });
        var vm = base.extendTo(this);

        // Extend the existing home controller
        // angular.extend(vm, base);
        // base.vm = vm;

        // Add custom definitions here
    }

})();
