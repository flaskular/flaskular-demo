/**
 * Routes for the dashboard page.
 *
 * @namespace app.dashboard
 */
(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .run(runBlock);

    runBlock.$inject =
        ['routerHelper', '$rootScope', '$state'];

    /**
     * Router initialization.
     */
    function runBlock(routerHelper, $rootScope, $state) {
        routerHelper.configureStates(getStates());
    }

    /**
     * The default state routes.
     */
    function getStates() {
        return [
            {
                state: 'dashboard',
                config: {
                    templateUrl: 'static/dashboard/dashboard.html',
                    url: '/dashboard',
                    // controller: 'HomeController',
                    // controllerAs: 'vm',
                    loginRequired: true
                }
            },
        ];
    }
})();
