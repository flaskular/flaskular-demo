/**
 * Module definition for dashboard. Replaces the Flaskular default dashboard,
 * which does not have very much.
 *
 * @namespace app.dashboard
 */
(function() {
    'use strict';

    angular
        .module('app.dashboard', [
            'ui.router',
        ]);
})();
