/**
 * Routes for the dashboard page.
 */
(function() {
    'use strict';

    angular
        .module('app.example')
        .run(runBlock);

    runBlock.$inject =
        ['routerHelper', '$rootScope', '$state'];

    /**
     * Router initialization.
     */
    function runBlock(routerHelper, $rootScope, $state) {
        routerHelper.configureStates(getStates());
    }

    /**
     * The default state routes.
     */
    function getStates() {
        return [
            {
                // Note: for more advance home pages, break into a new module
                state: 'example',
                config: {
                    templateUrl: 'static/example/example.html',
                    url: '/example',
                    controller: 'ExampleController',
                    controllerAs: 'vm',
                }
            },
        ];
    }
})();
