/**
 * Service for agent, connecting to the flask server.
 *
 * @namespace app.agent
 */
(function() {
    'use strict';

    angular
        .module('app.example')
        .service('exampleService', exampleService);

    exampleService.$inject = ['$q', 'authService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Agent service definition.
     */
    function exampleService($q, authService) {

        var service = {
            // Methods
            hello : hello
        };

        return service;

        /////////////////////////////////////////////////////////////
        //  Backend Connections
        /////////////////////////////////////////////////////////////

        /**
         * Attempts to create a new agent, given the agent info.
         *
         * @param {dict} agent The info required to create a new agent.
         */
        function hello(foo) {
            return authService.post('/example/hello', {foo: foo});
        }
    }
})();
