/**
 * The league edit view controller.
 *
 * @namespace app.league
 */
(function() {
    'use strict';

    angular
        .module('app.example')
        .controller('ExampleController', ExampleController);

    ExampleController.$inject =
        ['$state', 'exampleService'];

    ///////////////////////////////////////////////////////////////////////////

    function ExampleController($state, exampleService) {
        var vm = this;

        // data
        vm.myMsg = 'wrong';
        vm.foo = 'something';

        vm.initialize = initialize;

        initialize();

        /////////////////////////////////////
        //  Initialization
        /////////////////////////////////////

        /**
         * Initializes the edit view.
         *
         * @param {int or str} leagueId The league id to initialize.
         */
        function initialize() {

            // Handle a successful fetch of league info
            function handleSuccess(data) {
                vm.myMsg = data['my_msg'];
            }

            // Handle a failed fetch of league info
            function handleError(data) {
                $state.go('error', data, {});
            }

            exampleService.hello(vm.foo)
                .then(handleSuccess)
                .catch(handleError);
        }

    }
})();
