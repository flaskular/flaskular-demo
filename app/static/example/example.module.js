/**
 * Module definition for league.
 *
 * A league manages competitions, including start/end times, data sources,
 * rules, etc.
 *
 * @namespace app.league
 */
(function() {
    'use strict';

    angular
        .module('app.example', [
            'ui.router',
            'flaskular.router'
        ]);
})();
