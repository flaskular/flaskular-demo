# Sections #

* [Starting a New Project](#starting-a-new-project)
* [Ubuntu Installation](#ubuntu-installation)
* [Getting Started](#getting-started)

# Starting a New Project #

Clone the `flaskular` framework and the `flaskular-bootstrap` project repos
into the desired location, giving `flaskular-bootstrap` a custom name
representing your project.

We recommended that you put flaskular is in a directory named `flaskular`
that is a sibling directory to your new project. Otherwise, the karma tests
will not be able to find the core flaskular js libraries that it needs to run,
and you will need to change `flaskularDir` on line 3 of `karma.conf.js` to
point to where you installed flaskular.

        $ git clone git@gitlab.com:flaskular/flaskular.git /path/to/flaskular
        $ git clone git@gitlab.com:flaskular/flaskular-bootsrap.git /path/to/new-project

Note: from here on, `new-project` will be assumed to be the name you have chosen for
your project.

Enter the new project and change the remote refs:

        $ cd /path/to/new-project
        $ git remote set-url origin git@host:group/new-project.git
        $ git push origin master

# Ubuntu Installation #

These instructions assume an Ubuntu distribution (tested on 14.04 - Trusty).

## Installing Python 3 ##

Install python 3 and the dev libraries with:

        $ sudo apt-get install python3 python3-dev libffi-dev nginx python-requests

## Setting Up the Virtual Environment ##

First, we need to create a dedicated virtual environment for this project. We
will use virtualenvwrapper, which we install globally.

        $ sudo pip install virtualenv virtualenvwrapper

Note, there are other steps required for the installation of
virtualenvwrapper. Follow the complete guide
[http://virtualenvwrapper.readthedocs.org/en/latest/install.html](http://virtualenvwrapper.readthedocs.org/en/latest/install.html).

Find the location of the python 3 installation:

        $ which python3
        /usr/bin/python3

Create the virtual environment `new-project` pointing to the python 3
installation above:

        $ mkvirtualenv --python=/usr/bin/python3 new-project

This script will activate the virtual environment `new-project`
automatically. If you need to activate it in the future, just execute the
following command:

        $ workon new-project

For convenience, you can configure the environment to navigate directly to
the project structure as soon as the environment is loaded. To do so, open
the postactivate script for the environment in a text editor:

        $ vim $VIRTUAL_ENV/bin/postactivate

And add the following lines to the script:

        cd ~/path/to/the/project

## Installing the Database - PostgreSQL ##

Follow the instructions
[http://www.postgresql.org/download/linux/ubuntu/](http://www.postgresql.org/download/linux/ubuntu/) to install
PostgreSQL and the dev server.

        $ sudo apt-get install postgresql-9.4 postgresql-server-dev-9.4

## Installing the Client-Side Dependencies ##

Next, install the front-end dependencies (NodeJS and Bower):

        $ sudo apt-get install curl
        $ curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
        $ sudo apt-get install --yes nodejs

        $ npm install -g bower

## Configure the Environment ##

We now need to configure the environment. Open the `postactivate` file:

        $ vim $VIRTUAL_ENV/bin/postactivate

Add the following line, which will tell the project to run in a development
configuration:

        export APP_SETTINGS="config.DevelopmentConfig"

Note, if the project should be run in staging mode, replace `DevelopmentConfig`
with `StagingConfig`. Similarlty, if the project should be run in production
mode, replace `DevelopmentConfig` with `ProductionConfig`.

While in the file, also add the following line to configure PostgreSQL:

        export DATABASE_URL="postgresql:///new_project_dev"

If the environment is running, restart the environment to load the changes.

        $ deactivate
        $ workon new-project

## Install Project Dependencies ##

First install `flaskular` (within the `new-project` virtual environment) by
navigating to the `flaskular` directory and running:

        $ python setup.py develop

You may want to run the `flaskular` tests to make sure everything installed
correctly:

        $ python setup.py test

Now, navigate back to the `new-project` directory and install this project in
the same virtual environment with:

        $ python setup.py develop

For safety, run the develop script for each projects twice. Often, it fails the
first time.

## Migrate the Database ##

Create the database `new_project_dev` in PostgreSQL.

        $ psql
        create database new_project_dev; create database new_project_dev_test;

If running in the staging configuration, create database `new_project_stage`,
and if running in the production configuration, create database
`new_project_prod` with their corresponding test databases.

Run the model migration:

        $ python manage.py db init
        $ python manage.py db migrate
        $ python manage.py db upgrade

Note, the second command will need to be run every time the model changes.

## Running the Server ##

Activate the virtual environment `new-project` (if you have not already done
so).

        $ workon new-project

Then run the server with

        $ uwsgi --ini server_uwsgi.ini

Alternatively, if you want to run a simpler (but slower) server:

        $ python server.py

The server, by default runs on port 8000. If running on a public server, you
may need to do some additional configuration to make it accessible to the
public internet. There are many options, but one is to open the port in the
firewall. A simple script has been added to do this (note, the script uses
a `sudo` command and requires administrative priviliges):

        source openport.sh

This script may need to be run every time the machine reboots.

## Running the Unit Tests ##

The unit tests can be run with

        $ python setup.py test

Note, sometimes this script fails on the first run. Just run it a second time,
the problem usually resolves itself.

A report on test coverage will also be given; however, the report will only
include coverage for files for which test cases actually exist.

To run the tests only on a single module (either frontend or backend) within
app, use the following:

        $ python setup.py test --test-only=[module]

        -- OR --

        $ python setup.py test -t [module]

(where `[module]` is the name of the module to test, found in /app/[module]).

To run only the frontend (Angular) tests:

        $ python setup.py test --front-only

        -- OR --

        $ python setup.py test -f

To run only the backend (Python) tests:

        $ python setup.py test --back-only

        -- OR --

        $ python setup.py test -b

Note: the --back-only and the --front-only options (and their shortcuts) are
compatible with --test-only.

To see more information on options available in testing:

        $ python setup.py test --help

# Getting Started #

## Administrative Accounts ##

One administrative account is created by default, with email/username
`admin@example.com` and password `admin`. When you run the server, if this
account is still using its default password, a warning will be printed to
the command line. To fix this and make the system more secure, you will want to
change the password. To do this, log in to the admin account through a web
client, navigate to "Profile" in the top navigation, and fill out the "Change
Password" form at the bottom of the page.

You may alternatively wish to create
a new administrative account and delete this old one (you cannot do this
with the `ticker` account, TDF will always create a new one if it sees the
old one does not exist). To create a new admin account while deleting the old
one:

1. Register for a new account that you desire to be the administrative account.
2. Log out of the new admin account and log in as `admin@example.com`
3. Navigate to "Admin > User List"
4. In the row containing the new account, click on the "Promote" link in order
to promote the new user to an admin.
5. Log out from the `admin@example.com` account and log in as the new admin.
6. Navigate again to "Admin > User List".
7. Next to `admin@example.com` click on the "X" link to delete the old admin.
