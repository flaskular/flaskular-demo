from flaskular_manage.config import (
    Config as FConfig,
    ProductionConfig as FProductionConfig,
    ProductionConfigTesting as FProductionConfigTesting,
    StagingConfig as FStagingConfig,
    StagingConfigTesting as FStagingConfigTesting,
    DevelopmentConfig as FDevelopmentConfig,
    DevelopmentConfigTesting as FDevelopmentConfigTesting
)


class Config(FConfig):
    SECRET_KEY = 'changeme'
    CSRF_SESSION_KEY = 'changeme'

    # Custom general configuration here
    # ...


class ProductionConfig(Config, FProductionConfig):
    # Custom production configuration here
    # ...
    pass


class ProductionConfigTesting(ProductionConfig, FProductionConfigTesting):
    # Custom configuration here
    # ...
    pass


class StagingConfig(Config, FStagingConfig):
    # Custom configuration here
    # ...
    pass


class StagingConfigTesting(StagingConfig, FStagingConfigTesting):
    # Custom configuraiton here
    # ...
    pass


class DevelopmentConfig(Config, FDevelopmentConfig):
    # Custom configuration here
    # ...
    pass


class DevelopmentConfigTesting(DevelopmentConfig, FDevelopmentConfigTesting):
    # Custom configuration here
    # ...
    pass
